<?php

namespace Database\Seeders;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $faker = \Faker\Factory::create();

        //генерация 2000 пользователей
        for ($i = 0; $i < 2000; $i++) {
            $name = $faker->name;
            $birthDate = $faker->date('d-m-Y');

            // Преобразование даты рождения в формат Carbon
            $parsedBirthDate = Carbon::createFromFormat('d-m-Y', $birthDate)->format('Y-m-d');

            User::create([
                'name' => $name,
                'birth_date' => $parsedBirthDate,
            ]);
        }
    }
}
