<?php

namespace Database\Seeders;

use App\Models\Phone;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PhonesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $faker = \Faker\Factory::create();

        for ($i = 1; $i <= 2000; $i++) {

            //количество номеров у пользователя
            $phonesСount = $faker->numberBetween(1, 3);
            $userData = [];

            for ($j = 1; $j <= $phonesСount; $j++) {
                //генерируем номер телефона в формате код страны (380) + код оператора (2 цифры) + номер (7 цифр)
                $phone = '380' . $faker->randomElement(['50', '67', '63', '68']) . $faker->numberBetween(1000000, 9999999);
                //баланс дял каждого номера в диапазоне от -50 до 150
                $balance = $faker->numberBetween(-50, 150);

                Phone::create([
                    'phone' => $phone,
                    'balance' => $balance,
                    'user_id' => $i
                ]);
            }
        }
    }
}
