<?php

namespace App\Services;

use App\Models\User;
use App\Models\Phone;

class UserService
{
    /**
     * @param $userId
     * @return array
     */
    public function getUserData($userId): array
    {
        $userData = User::findOrFail($userId)->toArray();
        $userPhones = Phone::where('user_id', $userId)->pluck('phone')->toArray();
        $userData['phones'] = implode(', ', $userPhones);

        return $userData;
    }

    /**
     * @param $phoneId
     * @param $amount
     * @return float
     */
    public function topUpPhoneNumber($phoneId, $amount): float
    {
        $phone = Phone::findOrFail($phoneId);
        $newBalance = $phone->balance + $amount;
        $phone->balance = $newBalance;
        $phone->save();

        return $newBalance;
    }

    /**
     * @param $name
     * @param $birth_date
     * @return User
     */
    public function createUser($name, $birth_date): User
    {
        return User::create([
            'name' => $name,
            'birth_date' => $birth_date
        ]);
    }

    /**
     * @param $userId
     * @param $phone
     * @return Phone
     */
    public function addPhone($userId, $phone): Phone
    {

        return Phone::create([
            'user_id' => $userId,
            'phone' => $phone,
            'balance' => 0
        ]);
    }

    /**
     * @param $userId
     * @return void
     */
    public function deleteUser($userId): void
    {
        Phone::where('user_id', $userId)->delete();
        User::where('id', $userId)->delete();
    }
}
