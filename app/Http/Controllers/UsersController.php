<?php

namespace App\Http\Controllers;

use App\Services\UserService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    private UserService $service;

    public function __construct(UserService $service)
    {
        $this->service = $service;
    }

    /**
     * получение данных пользователя
     * @param Request $request
     * @return JsonResponse
     */
    public function getUserData(Request $request): JsonResponse
    {
        $request->validate([
            'user_id' => 'required|integer|exists:App\Models\User,id'
        ]);

        $userId = $request->get('user_id');
        $userData = $this->service->getUserData($userId);

        return response()->json($userData);
    }

    /**
     * пополнение баланса
     * @param Request $request
     * @return JsonResponse
     */
    public function topUpPhoneNumber(Request $request)
    {
        $request->validate([
            'phone_id' => 'required|integer',
            'amount' => 'required|numeric|between:0,100'
        ]);
        $phoneId = $request->get('phone_id');
        $amount = $request->get('amount');

        $newBalance = $this->service->topUpPhoneNumber($phoneId, $amount);

        return response()->json(["new balance: " => $newBalance]);
    }

    /**
     * создание нового пользователя
     * @param Request $request
     * @return JsonResponse
     */
    public function createUser(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'birth_date' => 'required|date'
        ]);
        $name = $request->get('name');
        $birth_date = $request->get('birth_date');

        $newUser = $this->service->createUser($name, $birth_date);

        return response()->json($newUser);
    }

    /**
     * добавление теелефона
     * @param Request $request
     * @return JsonResponse
     */
    public function addPhone(Request $request)
    {
        $request->validate([
            'user_id' => 'required|integer|exists:App\Models\User,id',
            'phone' => 'required|string|digits:12'
        ]);
        $userId = $request->get('user_id');
        $phone = $request->get('phone');

        $newPhone = $this->service->addPhone($userId, $phone);

        return response()->json($newPhone);
    }

    /**
     * удаление пользователя и его телефонов
     * @param Request $request
     * @return JsonResponse
     */
    public function deleteUser(Request $request)
    {
        $request->validate([
            'user_id' => 'required|integer|exists:App\Models\User,id',
        ]);

        $userId = $request->get('user_id');

        $this->service->deleteUser($userId);

        return response()->json();
    }
}
