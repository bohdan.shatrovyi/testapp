/*1*/
SELECT u.id, u.name, SUM(p.balance) AS total_balance
FROM users u
JOIN phones p ON u.id = p.user_id
GROUP BY u.id, u.name;

/*2*/
SELECT SUBSTRING(p.phone, 3, 3) as operator, COUNT(*)  as phones_count
FROM phones p
GROUP BY operator

/*3*/
SELECT u.name, COUNT(p.phone) AS phone_count
FROM users u
JOIN phones p ON u.id = p.user_id
GROUP BY u.name;

/*4*/
SELECT u.name,  MAX(p.balance) AS max_balance
FROM users u
JOIN phones p ON u.id = p.user_id
GROUP BY u.name
ORDER BY max_balance DESC
LIMIT 10;