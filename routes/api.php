<?php

use App\Http\Controllers\UsersController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::get('/get-user', [UsersController::class, 'getUserData']);
Route::get('/top-up', [UsersController::class, 'topUpPhoneNumber']);
Route::get('/create-user', [UsersController::class, 'createUser']);
Route::get('/add-phone', [UsersController::class, 'addPhone']);
Route::get('/delete', [UsersController::class, 'deleteUser']);
